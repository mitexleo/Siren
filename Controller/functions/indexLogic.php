<?php
//Feedback
if (isset($_POST['fedsubmit'])) {
  mail('team@jojoyou.org', 'PriEco Feedback', 'Email: ' . $_POST['fedEmail'] . '
Feedback: ' . $_POST['fedsug'].'
Search Query: '. $purl);
}
//Quick Settings Buttons
if (isset($_POST['allBut'])) {
  header("Location: ./?q=" . urlencode($_POST['q']), true);
  exit();
}
if (isset($_POST['imgBut'])) {
  header('Location: ./?image&q=' . urlencode($_POST['q']), true);
  exit();
}
if (isset($_POST['videoBut'])) {
  header('Location: ./?video&q=' . urlencode($_POST['q']), true);
  exit();
}
if (isset($_POST['newsBut'])) {
  header('Location: ./?news&q=' . urlencode($_POST['q']), true);
  exit();
}
if (isset($_POST['mapBut'])) {
  header("Location: https://www.openstreetmap.org/search?query=" . urlencode($_POST['q']));
  exit();
}

//Save Settings
if (isset($_POST['savesetting'])) {
  if (isset($_POST['darkSlider']) && $_POST['darkSlider'] != '0') {
      setcookie('mode', $_POST['darkSlider'], time() + 31536000, '/');
  }
  else{
    setcookie('mode', null, -1, '/');
  }
  if ($_POST['customTheme'] and $_POST['customTheme'] != "") {
    setcookie('theme', $_POST['customTheme'], time() + 31536000, '/');
  }
  if (isset($_POST['LangDropdown'])) {
      setcookie('Language', $_POST['LangDropdown'], time() + 31536000, '/');
    
  }

  if (isset($_POST['newtab'])) {
    setcookie('new', 'on', time() + 31536000, '/');
  } else {
    setcookie('new', null, -1, '/');
  }
  if (isset($_POST['providers'])) {
    setcookie('providers', 'on', time() + 31536000, '/');
  } else {
    setcookie('providers', null, -1, '/');
  }
  if (isset($_POST['showtime'])) {
    setcookie('showtime', 'on', time() + 31536000, '/');
  } else {
    setcookie('showtime', null, -1, '/');
  }
  if (isset($_POST['datasave'])) {
    setcookie('datasave', 'on', time() + 31536000, '/');
  } else {
    setcookie('datasave', null, -1, '/');
  }
  if (isset($_POST['hQuery'])) {
    setcookie('hQuery', 'on', time() + 31536000, '/');
  } else {
    setcookie('hQuery', null, -1, '/');
  }
  if (!isset($_POST['dSug'])) {
    setcookie('DisSugges', 'on', time() + 31536000, '/');
  } else {
    setcookie('DisSugges', null, -1, '/');
  }
  if (!isset($_POST['dQue'])) {
    setcookie('DisQue', 'on', time() + 31536000, '/');
  } else {
    setcookie('DisQue', null, -1, '/');
  }
  if (isset($_POST['aCou'])) {
    setcookie('userid', rand(1000000, 1000000000000), time() + (86400 * 364), '/');
    setcookie('noanalytics', null, -1, '/');
  } else {
    setcookie('noanalytics', 'true', time() + (86400 * 364), '/');
    setcookie('userid', null, -1, '/');
  }
  if(isset($_POST['imP'])){
    setcookie('improvePriEco',  'on', time() + 31536000, '/');
  }
  else{
    setcookie('improvePriEco', null, -1, '/');
  }
  $reload = true;
}
if (isset($_POST['savequicksetting'])) {

  if (isset($_POST['LocDropDown'])) {
    if ($_POST['LocDropDown'] == "all") {
      setcookie('Location', 'all', time() + 31536000, '/');
    } else {
      setcookie('Location', $_POST['LocDropDown'], time() + 31536000, '/');
    }
  }

  if (isset($_POST['SafeDropDown'])) {
    if ($_POST['SafeDropDown'] == "off") {
      setcookie('safe', 'off', time() + 31536000, '/');
    } else {
      setcookie('safe', null, -1, '/');
    }
  }
  if (isset($_POST['TimeDropDown'])) {
    switch ($_POST['TimeDropDown']):

      case "day":
        setcookie('time', 'day', time() + 31536000, '/');
        break;
      case "week":
        setcookie('time', 'week', time() + 31536000, '/');
        break;
      case "month":
        setcookie('time', 'month', time() + 31536000, '/');
        break;
      case "year":
        setcookie('time', 'year', time() + 31536000, '/');
        break;
      default:
        setcookie('time', null, -1, '/');
        break;
    endswitch;
  }
  $reload = true;
}
if (isset($_POST['imgtoolsSave'])) {
  $purl = urlencode($purl);
  header('Location: ./?image&imgsize=' . $_POST['imgtoolsSize'] . '&imgcolor=' . $_POST['imgtoolsColor'] . '&imgtype=' . $_POST['imgtoolsType'] . '&imgtime=' . $_POST['imgtoolsTime'] . '&imglicence=' . $_POST['imgtoolsRights'] . '&q=' . $purl, true);
  exit();
}
if(isset($_POST['pixabayimg'])){
  $purl = urlencode($purl);
  header('Location: ./?image&pixabay&q='. $purl);
  exit();
}
if (isset($_POST['imgback'])) {
  $purl = urlencode($purl);
  header('Location: ./?image&q=' . $purl);
  exit();
}
//Reload
if ($reload) {
  $purl = urlencode($purl);
  if(strpos($url, '?image') != false){
    header("Location: ./?image&q=" . $purl, true);
    exit();
  }
  elseif(strpos($url, '?video') != false){
    header("Location: ./?video&q=" . $purl, true);
    exit();
  }
  elseif(strpos($url, '?news') != false){
    header("Location: ./?news&q=" . $purl, true);
    exit();
  }
  else {
    header("Location: ./?q=" . $purl, true);
    exit();
  }
}
