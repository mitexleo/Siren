<!--  SPDX-FileCopyrightText: 2022, 2022-2022 Roman  Láncoš <jojoyou@jojoyou.org> -->
<!-- -->
<!--  SPDX-License-Identifier: AGPL-3.0-or-later -->

<?php
$apiPurl = str_replace(' ','+',$purl);

if (strpos($purl, 'weather ') !== false) {
$OpenWeatherLoc = str_replace('weather ', '', $purl);
}
else{$OpenWeatherLoc = '';}

$newsdate = date('m/d/Y');
$newsdate = strtotime($newsdate);
$newsdate = strtotime("-7 day", $newsdate);

if (strpos($purl, 'def') !== false && !$dev) {$defWords = str_replace(' ', '%20', preg_replace('/\b\w*def\w*\b/', '', $purl));}
$tmp = $lang;
if($tmp==null or $tmp == 'all'){
    $tmp = 'en';
}

if(!$dev) {
    //API keys ($_ENV variables loaded in database.php file) 
    $Googlefile = 'https://www.googleapis.com/customsearch/v1?key='.$_ENV['GOOGLE_API_KEY'].'&cx='.$_ENV['GOOGLE_CX_KEY'].'&hl='.$lang.'&gl='.$loc.'&dateRestrict='.$date.'&safe='.$safe.'&q='.$apiPurl;

    $EtsyFile = 'https://openapi.etsy.com/v3/application/listings/active?limit=25&keywords='.$apiPurl;
    $OpenWeatherFile = 'https://api.openweathermap.org/data/2.5/weather?appid='.$_ENV['OPENWEATHER_API_KEY'].'&q='.$OpenWeatherLoc;
    if(isset($defWords)){$WordnikFile = 'https://api.wordnik.com/v4/word.json/'.$defWords.'/definitions?limit=5&includeRelated=false&useCanonical=false&includeTags=false&api_key='.$_ENV['WORDNIK_API_KEY'];}
    else{$WordnikFile ='';}
    $RedditFile = 'https://api.reddit.com/r/all/search?q='.$apiPurl.'&limit=4&sort=relevance';
    $DdgFile = 'https://api.duckduckgo.com/?format=json&q='.$apiPurl;

    $PixabayFile = 'https://pixabay.com/api/?key='.$_ENV['PIXABAY_API_KEY'].'&per_page=200&q='.$apiPurl;
    $YoutubeFile = 'https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&maxResults=50&key='.$_ENV['YOUTUBE_API_KEY'].'&q='.$apiPurl;
    $NewsFile = 'https://newsapi.org/v2/everything?apiKey='.$_ENV['NEWS_API_KEY'].'&from='.date('Y-m-d', $newsdate).'&searchIn=title&sortBy=popularity&language='.$tmp.'&q='.$apiPurl;
}
else{
    //Null API keys    
    $Googlefile = './Controller/dev/google.json';
    $MojeekFile = './Controller/dev/mojeek.json';
    
    $OpenWeatherFile='./Controller/dev/openweather.json';
    $RedditFile = './Controller/dev/reddit.json';
    $DdgFile = './Controller/dev/ddg.json';

    $PixabayFile = './Controller/dev/pixabay.json';
    $YoutubeFile = './Controller/dev/yt.json';
    $NewsFile = './Controller/dev/news.json';
}

$PromoURL = './Controller/value/data.json';