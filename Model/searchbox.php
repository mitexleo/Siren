<?php
$langName = [""];
$langVal = [""];
foreach ($promoobj['lang'][0] as $location) {
  array_push($langVal, $location);
}
foreach ($promoobj['lang'][0] as $name => $value) {
  array_push($langName, $name);
}
$locName = [""];
$locVal = [""];
foreach ($promoobj['loc'][0] as $location) {
  array_push($locVal, $location);
}
foreach ($promoobj['loc'][0] as $name => $value) {
  array_push($locName, $name);
}


echo '

<div
class="scrollSearchBackground" 
style="backdrop-filter: blur(20px);
width:100%; height:80px; position:fixed; z-index:9;
border-radius:0 0 20px 20px;
">
</div>


<form class="searchForm" id="searchForm" method="post" >

<input type="hidden" name="search_type" value="'.$type.'">
<div class="searchM">

  <div class="autocomplete">
  <input list="suggestions"
   id="searchBox"
   value="' , htmlspecialchars($purl, ENT_QUOTES | ENT_HTML5, 'UTF-8'), '"
   class="searchBox"
   name="q"
   size="21"
   placeholder="PriEco"
   autocomplete="off"
/>
';

if(!isset($_COOKIE['DisQue'])){
echo '<button class="delQueryBtn" onclick="delFuc()" type="button">X</button>';
}

echo '
<button id="searchButton" class="searchButton">';
  if(!isset($_COOKIE['datasave'])){
    echo '<img alt="icnSearch" src="./View/icon/search.webp" style="width:10px; height:10px;">';
  }
  else{
    echo '<p>➤</p>';
  }
  echo '</button><div class="autocom-box">
        </div>
 </div>
 
 </div>

</form>

<div class="topbg" id="topbg" style="
position: absolute;
width: 100%;
z-index: 10;
backdrop-filter: blur(20px);
border-radius:0 0 20px 20px;
">
<br>
<a href="/" style="text-decoration:none;"
 >
 ';
 if(!isset($_COOKIE['datasave'])){
  echo '<img class="sLogo"alt="TreeLogo"
  src="./View/img/PriEco.webp" />';
 }else{
  echo '<h1 class="sLogo" style="color: #0ed794;text-decoration: none;font-size: 60px;">P</h1>';
 } 
 echo '</a>


<div class="settingButtons" style="margin-top: 60px;display: flex;
overflow-x: auto;
overflow-y: hidden;">
<form method="POST" action="">
<input type="hidden" name="search_type" value="">
<input type="hidden" name="q" value="', $purl ,'">

<button style="';
if ($type != 'image' and $type != 'video' and $type !='news') {
  echo 'border-image: linear-gradient(0.25turn, rgb(51, 255, 34), rgb(2, 141, 155));
border-image-slice: 1;color:#039103;border-bottom: #bbb solid;';
}
echo '"class="allBut settingButton" style="margin-left:2%;"';
if(!isset($_COOKIE['hQuery'])){
  echo 'name="allBut" id="allbut"';
}
echo '>';
  if(!isset($_COOKIE['datasave'])){
    echo '<img src="./View/icon/search.webp" alt="" class="setBut" style="'; 
    if ($type != 'image' and $type != 'video' and $type !='news') {
      echo 'filter: invert(28%) sepia(82%) saturate(4091%) hue-rotate(117deg) brightness(96%) contrast(104%);';
    }
    echo '">';
  }
  if($lang == null || $lang == 'en' || $lang == 'all'){echo 'All';}
  else{echo $promoobj['All'][0][$lang];}
  echo '</button>
  </form>  


  <form method="POST" action="">
<input type="hidden" name="search_type" value="image">
<input type="hidden" name="q" value="', $purl ,'">
<button style="';
if ($type == 'image') {
  echo 'border-image: linear-gradient(0.25turn, rgb(51, 255, 34), rgb(2, 141, 155));
  border-image-slice: 1;color:#039103;color:#039103;border-bottom: #bbb solid;';
}
echo '"class="settingButton" ';
if(!isset($_COOKIE['hQuery'])){
  echo 'name="imgBut" id="imgbut"';
}
echo '>';
if(!isset($_COOKIE['datasave'])){
  echo '<img src="./View/icon/img.webp" alt="" class="setBut" style="'; 
  if ($type == 'image') {
    echo 'filter: invert(28%) sepia(82%) saturate(4091%) hue-rotate(117deg) brightness(96%) contrast(104%);';
  }
  echo '">';
}
  if($lang == null || $lang == 'en' || $lang == 'all'){echo 'Images';}
  else{echo $promoobj['Images'][0][$lang];}
  echo '</button></form>

  <form method="POST" action="">
<input type="hidden" name="search_type" value="video">
<input type="hidden" name="q" value="', $purl ,'">
  <button style="';
  if ($type == 'video') {
    echo 'border-image: linear-gradient(0.25turn, rgb(51, 255, 34), rgb(2, 141, 155));
    border-image-slice: 1;color:#039103;color:#039103;border-bottom: #bbb solid;';
  }
  echo '"class="settingButton"';
  if(!isset($_COOKIE['hQuery'])){
    echo 'name="videoBut"';
  }
  echo '>';
  if(!isset($_COOKIE['datasave'])){
    echo '<img src="./View/icon/video.webp" alt="" class="setBut" style="'; 
    if ($type == 'video') {
      echo 'filter: invert(28%) sepia(82%) saturate(4091%) hue-rotate(117deg) brightness(96%) contrast(104%);';
    }
    echo '">';
  }
    if($lang == null || $lang == 'en' || $lang == 'all'){echo 'Videos';}
    else{echo $promoobj['Videos'][0][$lang];}
    echo '</button></form>

    <form method="POST" action="">
    <input type="hidden" name="search_type" value="news">
    <input type="hidden" name="q" value="', $purl ,'">
  <button style="';
  if ($type == 'news') {
    echo 'border-image: linear-gradient(0.25turn, rgb(51, 255, 34), rgb(2, 141, 155));
    border-image-slice: 1;color:#039103;color:#039103;border-bottom: #bbb solid;';
  }
  echo '"class="settingButton"';
  if(!isset($_COOKIE['hQuery'])){
    echo 'name="newsBut"';
  }
  echo '>';
  if(!isset($_COOKIE['datasave'])){
    echo '<img src="./View/icon/news.webp" alt="" class="setBut" style="'; 
    if ($type == 'news') {
      echo 'filter: invert(28%) sepia(82%) saturate(4091%) hue-rotate(117deg) brightness(96%) contrast(104%);';
    }
    echo '">';
  }
    if($lang == null || $lang == 'en' || $lang == 'all'){echo 'News';}
    else{echo $promoobj['News'][0][$lang];}
    echo '</button></form>
<a style="text-decoration: none;" href="https://www.openstreetmap.org/search?query=',$purl,'"><button class="settingButton" id="mapbut">';
if(!isset($_COOKIE['datasave'])){
  echo '<img src="./View/icon/map.webp" alt="" class="setBut">';
}
  if($lang == null || $lang == 'en' || $lang == 'all'){echo 'Map';}
  else{echo $promoobj['Map'][0][$lang];}
  echo '</button></a>
  </form>
  <input type="checkbox" id="feedbackMenu" hidden>
<label class="feedbackMenuLabel" for="feedbackMenu">';
if(!isset($_COOKIE['datasave'])){
echo '<img src="./View/icon/feedback.webp" alt="feedback" >';
}
else{
  echo '<p class="settingButton" style="margin-left: 0;">Feedback</p>';
}
echo '</label>
<div class="feedback">
<form method="POST" action="">
<h4>Thank you for helping improve PriEco</h4>
<input type="text" name="fedEmail" placeholder="Email" style="border-radius: 10px;
border: solid 2px #f2f2f2;
padding: 3px;">
<p style="font-style: italic;font-size: 12px;"><b>Not required</b> just if you want us to contact you back</p>
<br><textarea name="fedsug" style="border-radius: 20px;
border: solid 2px #f2f2f2;padding: 10px;
width: 100%;
height: 70px;" placeholder="Feedback*" requiered></textarea>
<p style="font-style: italic;font-size: 12px;">Your current query will be sent with your feedback</p>
<input type="submit" name="fedsubmit"value="Submit" style="border: solid 2px lightgray;
padding: 10px;
border-radius: 20px;
cursor: pointer;
float: left;
font-weight: normal;
margin-left: 37%;
margin-top: 10px;">
</form>
<br>
  </div>

  </div>
</div>
</form>
<button class="tree-btn" id="tree-btn"><img alt="icntree" src="./View/icon/user.svg" style="width:20px;height:20px;"><p style="font-weight:bold;">'
, $usr , '</p></button>
  <br>
';

include 'settings.php';

echo '
<input type="checkbox" id="threedotsquick" hidden>
<label for="threedotsquick" class="labelforcheckquick">...</label>
<div class="topspace"></div>
<form method="post" action="" class="quickSettingButtons">
<select style="margin-left:9vw;" name="LocDropDown" class="quickSet">
<option disabled selected hidden>';
if ($loc !== null) {
  $i = array_search($loc, $locVal);
  echo $locName[$i];
}
echo '
  </option>
  <option value="all"';
if ($loc == 'all' || $loc == null) {
  echo 'selected';
}
echo '>All regions</option>
  ';
$i = 0;
foreach ($locName as $locs) {
  echo "<option value='" . $locVal[$i] . "'>" . $locName[$i] . "</option>";
  $i++;
}
echo '
  </select>

  <select name="SafeDropDown" class="quickSet">
  <option disabled selected hidden>';
  if(isset($_COOKIE['safe'])){
    switch ($_COOKIE['safe']) {
      case "active":
        echo "SafeSearch: On";
        break;
      case "off":
        echo "SafeSearch: Off";
        break;
    }
  }
  else{echo "SafeSearch: On";}
echo '
    </option>
    <option value="active">On</option>
    <option value="off">Off</option>
</select>

<select name="TimeDropDown" class="quickSet">
<option disabled selected hidden>';
if(isset($_COOKIE['time'])){
switch ($_COOKIE['time']):
  case "day":
    echo "Past day";
    break;
  case "week":
    echo "Past week";
    break;
  case "month":
    echo "Past month";
    break;
  case "year":
    echo "Past year";
    break;
endswitch;
}
else{echo "Any time";}
echo '
    </option>
    <option value="any">Any time</option>
    <option value="day">Past day</option>
    <option value="week">Past week</option>
    <option value="month">Past month</option>
    <option value="year">Past year</option>

</select>

<input type="submit" name="savequicksetting" class="quickSet" value="Save">
</form>
<div class="quickSettingsSpace"></div>
';
